public class Jackpot {
	public static void main (String[] arg) {
		//Greeting message
		System.out.println("Welcome to Shut the Box Game");
		
		//Board object
		Board board = new Board();
		
		//boolean variable
		boolean gameOver = false;
		//int variable
		int numOfTilesClosed = 0;
		
		//loop that repeats while gameOver is not true
		while(!gameOver) {
			//print board object
			System.out.println(board);
			//Call playATurn() and check if it returns true
			if (board.playATurn() == true) {
				gameOver = true;
			}
			//Call playATurn() and check if it returns true
			else {
				numOfTilesClosed = numOfTilesClosed + 1;
			}
		}
		
		//check numOfTilesClosed is >= 7 and write if, else statements
		if (numOfTilesClosed >= 7) {
			System.out.println("Jackpot! You win this game");
		}else {
			System.out.println("You lose. Try one more game!");
		}

	}
}