public class Board {
	
	//fields
	private Die d1;
	private Die d2;
	private boolean[] tiles;

	//constructor
	public Board() {
		this.d1 = new Die();
		this.d2 = new Die();
		this.tiles = new boolean[12];
		for (int i = 0; i < this.tiles.length; i++) {
			this.tiles[i] = false;
		}
	}
	
	//override method - toString
	public String toString() {
		String emptyString = " ";
		for (int i = 0; i < this.tiles.length; i++) {
			if (!this.tiles[i]) {
				emptyString = emptyString + (i + 1) + " ";
			}
			else {
				emptyString = emptyString + "X ";
			}
		}
		return emptyString;														
	}
	
	//instance method playATurn()
	public boolean playATurn() {
		//call roll method on d1 and d2
		d1.roll();
		d2.roll();
		//print value of d1 and d2
		System.out.println("The value of Die 1 : " + d1 + " is rolled");
		System.out.println("The value of Die 2 : " + d2 + " is rolled");
		//sum of d1 and d2
		int sumOfDice = d1.getFaceValue() + d2.getFaceValue();
		
		if (!tiles[sumOfDice - 1]) {
			tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			System.out.println("-----------------------------");
			return false;
		}
		else if (!tiles[d1.getFaceValue()]) {
				tiles[d1.getFaceValue() - 1] = true;
				System.out.println("Closing tile with the same value as die one: "  + d1.getFaceValue());
				System.out.println("-----------------------------");
				return false;
		}
		else if (!tiles[d2.getFaceValue()]){
				tiles[d2.getFaceValue() - 1] = true;
				System.out.println("Closing tile with the same value as die two: "  + d2.getFaceValue());
				System.out.println("-----------------------------");
				return false;
		}
		
		else if ((tiles[sumOfDice - 1] == true) &&
				(tiles[d1.getFaceValue()] == true) &&
				(tiles[d2.getFaceValue()] == true)) {
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
		return tiles[sumOfDice - 1];
	}	
}