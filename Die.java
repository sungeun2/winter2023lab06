import java.util.Random;
public class Die {
	
	//fields
	private int faceValue;
	private Random random;
	
	//constructor
	public Die() {
		this.faceValue = 1;
		this.random = new Random();
	}
	
	//get method for faceValue field
	public int getFaceValue() {
		return this.faceValue;
	}
	
	//helper method - roll(), roll the dice between 1 and 6.
	public void roll() {
		this.faceValue = random.nextInt(6)+1;
	}
	
	//override method - toString
	public String toString() {
		return faceValue + " ";
	}
}